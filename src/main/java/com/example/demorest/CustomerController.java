package com.example.demorest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
 
@RestController
public class CustomerController
{
    @RequestMapping("/")
    public List<Customer> findAll()
    {
      List<Customer> customerList = new ArrayList<Customer>();
      customerList.add(new Customer(1, "frank"));
      customerList.add(new Customer(2, "john"));
      return customerList;
    }
    @RequestMapping("/courses")
    public List<Customer> findCourses()
    {
      List<Customer> customerList = new ArrayList<Customer>();
      customerList.add(new Customer(1, "frank2"));
      customerList.add(new Customer(2, "john2"));
      return customerList;
    }
    @RequestMapping("/example")
    public List<Customer> findCourses2()
    {
      List<Customer> customerList = new ArrayList<Customer>();
      customerList.add(new Customer(1, "frank3"));
      customerList.add(new Customer(2, "john3"));
      return customerList;
    }
}